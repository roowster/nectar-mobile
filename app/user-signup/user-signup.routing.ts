import { ModuleWithProviders }  from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { UserSignupComponent } from "./user-signup.component";

const userSignupRoutes: Routes = [
  { path: "user-signup", component: UserSignupComponent },
];
export const userSignupRouting: ModuleWithProviders = RouterModule.forChild(userSignupRoutes);