import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { userSignupRouting } from "./user-signup.routing";
import { UserSignupComponent } from "./user-signup.component";

@NgModule({
  imports: [
    NativeScriptFormsModule,
    NativeScriptCommonModule,
    NativeScriptRouterModule,
    userSignupRouting,
  ],
  declarations: [
    UserSignupComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class UserSignupModule { }