import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { Router } from "@angular/router";
import { UserService } from "../shared/user/user.service";
import { connectionType, getConnectionType } from "connectivity";
import { User } from "../shared/user/user";
import { BusinessService } from "../shared/business/business.service";

@Component({
  selector: 'app-user-signup',
  moduleId: module.id,
  providers: [UserService, BusinessService],
  templateUrl: './user-signup.component.html',
  styleUrls: ['./user-signup.component.css']
})
export class UserSignupComponent implements OnInit {
  user:User;
  isLoggingIn = true;
  isAuthenticating = false;
  constructor(private page: Page, private router: Router,
    private userService: UserService,
    private businessService: BusinessService) {
    this.user=new User();
  }


  ngOnInit() {
    this.page.actionBarHidden = true;
  }

  goBack() {

    this.router.navigate(['/pre-signup'])
  }
  submit() {

    if (!this.user.isValidEmail()) {
      alert("Enter a valid email address.");
      return;
    }

      this.signUp();
    
  }

  signUp() {
    if (getConnectionType() === connectionType.none) {
      alert("Groceries requires an internet connection to register.");
      return;
    }

    this.userService.register(this.user)
      .subscribe(
        () => {
          alert("Your account was successfully created.");
          this.isAuthenticating = false;
          
        },
        (message) => {
          // TODO: Verify this works
          if (message.match(/same user/)) {
            alert("This email address is already in use.");
          } else {
            alert("Unfortunately we were unable to create your account.");
          }
          this.isAuthenticating = false;
          this.router.navigate(['/login'])
        }
      );
  }

}

