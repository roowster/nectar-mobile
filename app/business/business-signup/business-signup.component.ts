import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { connectionType, getConnectionType } from "connectivity";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { UserService } from "../../shared/user/user.service";
import { Business } from "../../shared/business/business";
import { BusinessService } from "../../shared/business/business.service";
import { Animation } from "ui/animation";
import { View } from "ui/core/view";
import { prompt } from "ui/dialogs";
import { Page } from "ui/page";
import { TextField } from "ui/text-field";

@Component({
  selector: 'app-business-signup',
  moduleId: module.id,
  providers: [UserService,BusinessService],
  templateUrl: './business-signup.component.html',
  styleUrls: ['./business-signup.component.css']
})
export class BusinessSignupComponent implements OnInit {
 business:Business;
 isLoggingIn = true;
  isAuthenticating = false;
  constructor(private page: Page,
    private router: Router,
    private businessService: BusinessService) {
    page.actionBarHidden = true;
    this.business= new Business();
  }

  ngOnInit() {
  }

  goBack() {

    this.router.navigate(['/pre-signup'])
  }

  
  submit() {

    if (!this.business.user.isValidEmail()) {
      alert("Enter a valid email address.");
      return;
    }

      this.signUp();
    
  }

  signUp() {
    if (getConnectionType() === connectionType.none) {
      alert("Groceries requires an internet connection to register.");
      return;
    }

    this.businessService.register(this.business)
      .subscribe(
        () => {
          alert("Your account was successfully created.");
          this.isAuthenticating = false;
          
        },
        (message) => {
          // TODO: Verify this works
          if (message.match(/same user/)) {
            alert("This email address is already in use.");
          } else {
            alert("Unfortunately we were unable to create your account.");
          }
          this.isAuthenticating = false;
        }
      );
  }


}
