import { ModuleWithProviders }  from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { BusinessSignupComponent } from "./business-signup.component";

const businessSignupRoutes: Routes = [
  { path: "business-signup", component: BusinessSignupComponent },
];
export const businessSignupRouting: ModuleWithProviders = RouterModule.forChild(businessSignupRoutes);