import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { businessSignupRouting } from "./business-signup.routing";
import { BusinessSignupComponent } from "./business-signup.component";

@NgModule({
  imports: [
    NativeScriptFormsModule,
    NativeScriptCommonModule,
    NativeScriptRouterModule,
    businessSignupRouting
  ],
  declarations: [
    BusinessSignupComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class BusinessSignupModule { }