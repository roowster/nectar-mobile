
import { ModuleWithProviders }  from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PresignupComponent } from "./presignup.component";
import { AuthGuard } from "../auth-guard.service";

const presignupRoutes: Routes = [
//   { path: "pre-signup", component: PresignupComponent, canActivate: [AuthGuard] },
{ path: "pre-signup", component: PresignupComponent },
];
export const presignupRouting: ModuleWithProviders = RouterModule.forChild(presignupRoutes)