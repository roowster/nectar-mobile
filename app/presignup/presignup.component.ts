import { Component, OnInit } from '@angular/core';

import {Page} from "ui/page";
import { Router } from "@angular/router";

@Component({
  selector: 'app-presignup',
  moduleId:module.id,
  templateUrl: './presignup.component.html',
  styleUrls: ['./presignup.component.css']
})
export class PresignupComponent implements OnInit {

  constructor(private page: Page, private router:Router) { 
    page.actionBarHidden = true;
  }

  ngOnInit() {
  }

  goBack() {
    this.router.navigate(['/login'])
  }

}

