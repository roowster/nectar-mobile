import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { presignupRouting } from "./presignup.routing";
import { PresignupComponent } from "./presignup.component";
// import { GroceryListComponent } from "./grocery-list/grocery-list.component";
// import { ItemStatusPipe } from "./grocery-list/item-status.pipe";

@NgModule({
  imports: [
    NativeScriptFormsModule,
    NativeScriptCommonModule,
    NativeScriptRouterModule,
    presignupRouting,
  ],
  declarations: [
    PresignupComponent
    // GroceriesComponent,
    // GroceryListComponent,
    //ItemStatusPipe
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PresignupModule {}