import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { User } from "../shared/user/user";
import { UserService } from "../shared/user/user.service";
import { Router } from "@angular/router";

import { Color } from "color";
import { connectionType, getConnectionType } from "connectivity";
import { Animation } from "ui/animation";
import { View } from "ui/core/view";
import { prompt } from "ui/dialogs";
import { Page } from "ui/page";
import { TextField } from "ui/text-field";

@Component({
    selector: "gr-login",
    moduleId: module.id,
    providers: [UserService],
    templateUrl: "./login-common.html",
    // styleUrls: ["./login-common.css"]
    styleUrls: ["./login-common.css", "./login.component.css"],

})

export class LoginComponent implements OnInit {
    user: User;
    isLoggingIn = true;
    isAuthenticating = false;
    
    @ViewChild("initialContainer") initialContainer: ElementRef;
    @ViewChild("mainContainer") mainContainer: ElementRef;
    @ViewChild("logoContainer") logoContainer: ElementRef;
    @ViewChild("formControls") formControls: ElementRef;
    @ViewChild("signUpStack") signUpStack: ElementRef;
    @ViewChild("password") password: ElementRef;

    constructor(private router: Router,
        private userService: UserService,
        private page: Page) {
        this.user = new User();
    }

    ngOnInit() {
        this.page.actionBarHidden = true;
      }

     gotoPreSignUp() {
        console.log("gotoPreSignUp");
        // alert("You’re using: " + this.user.email);
        this.router.navigate(["/pre-signup"]);
    }

    submit() {
        console.log("hello");
        // alert("You’re using: " + this.user.email);
        this.router.navigate(["/items"]);

        if (this.isLoggingIn) {
            this.login();
        }

    }
    toggleDisplay() {
        this.isLoggingIn = !this.isLoggingIn;
    }

    login() {
        if (getConnectionType() === connectionType.none) {
            alert("Groceries requires an internet connection to log in.");
            return;
          }
        this.userService.login(this.user)
            .subscribe(
                () => this.router.navigate(["/"]),
                (error) => alert("Unfortunately we could not find your account.")
            );

    }
    // signUp() {
    //     this.userService.register(this.user)
    //         .subscribe(
    //             () => {
    //                 alert("Your account was successfully created.");
    //                 this.toggleDisplay();
    //             },
    //             () => alert("Unfortunately we were unable to create your account.")
    //         );
    // }

}
