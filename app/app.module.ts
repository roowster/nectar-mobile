import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { authProviders, appRoutes } from "./app.routing";
import { AppComponent } from "./app.component";
import { BackendService, LoginService, BusinessService } from "./shared";

import { HomeModule } from "./home/home.module";
import { LoginModule } from "./login/login.module";
import { PresignupModule } from "./presignup/presignup.module";
import { UserSignupModule } from "./user-signup/user-signup.module";
import { BusinessSignupModule } from "./business/business-signup/business-signup.module";


@NgModule({
  providers: [
    BackendService,
    LoginService,
    BusinessService,
    authProviders
  ],
  imports: [
    NativeScriptModule,
    NativeScriptHttpClientModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(appRoutes),
    HomeModule,
    LoginModule,
    PresignupModule,
    UserSignupModule,
    BusinessSignupModule
    
  ],
  declarations: [
      AppComponent,
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
