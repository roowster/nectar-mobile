import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';

@NgModule({
  imports: [
    CommonModule,
    NativeScriptFormsModule,
    NativeScriptCommonModule,
    UserRoutingModule
  ],
  declarations: [UserComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class UserModule { }
