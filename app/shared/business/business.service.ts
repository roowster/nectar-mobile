
import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { tap, map, catchError } from "rxjs/operators";

import { Business } from "./business";
import { BackendService } from "../backend.service";

@Injectable()
export class BusinessService {
  constructor(private http: HttpClient) {}
  register(business: Business) {
    return this.http.post(
      BackendService.baseUrl + "business/" ,
      JSON.stringify({
        username: business.email,
        email: business.email,
        password: business.password
      }),
      { headers: this.getCommonHeaders() }
    )
    .pipe(
      tap((data: any) => {
        BackendService.token = data.token;
      }),
      catchError(this.handleErrors)
    );
  }

  private getCommonHeaders() {
    return new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": BackendService.appUserHeader,
    });
  }

  private handleErrors(error: HttpErrorResponse) {
    console.log(JSON.stringify(error));
    return throwError(error);
  }


  login(business: Business) {
    return this.http.post(
      BackendService.baseUrl + "users/"  + "login",
      JSON.stringify({
        username: business.user.email,
        password: business.user.password
      }),
      { headers: this.getCommonHeaders() }
    )
    .pipe(
      tap((data: any) => {
        BackendService.token = data._kmd.authtoken;
      }),
      catchError(this.handleErrors)
    );
  }

}