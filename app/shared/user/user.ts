import { Component } from "@angular/core";
const validator = require("email-validator");


export class User {
    firstName:string;
    lastName:string;
    userName:string;
    email: string;
    password: string;
    phoneNumber: string;
    isValidEmail() {
    return validator.validate(this.email);
    }
  }

