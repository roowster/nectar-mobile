
import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { tap, map, catchError } from "rxjs/operators";


import { User } from "./user";
import { BackendService } from "../backend.service";



@Injectable()
export class UserService {
  constructor(private http: HttpClient) { }

  register(user: User) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    let data = {
      firstName: user.firstName,
      lastName: user.lastName,
      userName: user.userName,
      userEmail: user.email,
      phoneNumber: user.phoneNumber,
      password: user.password,
    };
    console.log(data);

    return this.http.post(
      BackendService.apiUrl + "users",
      JSON.stringify(data),
      { headers: this.getCommonHeaders() }
    )
    .pipe(catchError(this.handleErrors));
  }




  login(user: User) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post(

      BackendService.apiUrl + "users/" + "tokens",
      JSON.stringify({
        device_id: "native",
        login_id: user.email,
        userName: user.email,
        password: user.password
      }),
      { headers: this.getCommonHeaders() }
    ).pipe(
      tap((data: any) => {
        BackendService.token = data.token;
      }),
      catchError(this.handleErrors)
    );
  }

  logoff() {
    BackendService.token = "";
  }


  private getCommonHeaders() {
    return new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": BackendService.appUserHeader,
    });
  }

  private handleErrors(error: HttpErrorResponse) {
    console.log(JSON.stringify(error));
    return throwError(error);
  }

}