import { AuthGuard } from "./auth-guard.service";

export const authProviders = [
  AuthGuard
];

export const appRoutes = [
  { path: "", redirectTo: "/home", pathMatch: "full" }
];

// import { LoginComponent } from "./login/login.component";
// import { ItemsComponent } from "./item/items.component";
// import { PresignupComponent } from './presignup/presignup.component';
// import { BusinessSignupComponent } from './business-signup/business-signup.component';
// import { UserSignupComponent } from './user-signup/user-signup.component';
// import { AuthGuard } from "./auth-guard.service";


// export const authProviders = [
//   AuthGuard
// ];

// export const appRoutes = [
//     { path: "", component: LoginComponent },
//     { path: "items", component: ItemsComponent },
//     { path: "item", component: ItemsComponent },
//     { path: "login", component: LoginComponent },
//     { path: "pre-signup", component: PresignupComponent },
//     { path: "busines-signup", component: BusinessSignupComponent },
//     { path: "user-signup", component: UserSignupComponent },
//   ];

// export const navigatableComponents = [
//   AuthGuard,LoginComponent,ItemsComponent,PresignupComponent,BusinessSignupComponent,UserSignupComponent
//   ];

