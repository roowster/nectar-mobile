import { Component, OnInit } from "@angular/core";
import { User } from "../shared/user/user";
import { UserService } from "../shared/user/user.service";
import { Router } from "@angular/router";
import { Page } from "ui/page";

@Component({
    selector: "my-app",
    providers: [UserService],
    //templateUrl: "app.component.html",
    templateUrl: "./signup/signup-common.html",
    styleUrls: ["./signup/signup-common.css"]
})

export class SignupComponent {
    user: User;
    isLoggingIn = true;
    isAuthenticating = false;

    constructor(private router: Router, private userService: UserService,private page: Page) {
        this.user = new User();
    }

    ngOnInit() {
        this.page.actionBarHidden = true;
        
      }
    
    submit() {
        console.log("hello");
        // alert("You’re using: " + this.user.email);        
        this.router.navigate(["/items"]);
        
        if (this.isLoggingIn) {
            this.login();
        } else {
            this.signUp();
        }

    }
    toggleDisplay() {
        this.isLoggingIn = !this.isLoggingIn;
    }

    login() {
        this.userService.login(this.user)
            .subscribe(
                () => this.router.navigate(["/list"]),
                (error) => alert("Unfortunately we could not find your account.")
            );

    }
    signUp() {
        this.userService.register(this.user)
            .subscribe(
                () => {
                    alert("Your account was successfully created.");
                    this.toggleDisplay();
                },
                () => alert("Unfortunately we were unable to create your account.")
            );
    }

}
